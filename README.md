1. What is wrong with the JS, HTML, and CSS
	* Add image directory with sample pictures and correct names.
	* In the loop to change title text, the i variable needs to be bound to that function call.
		* Temporary solution without re-factoring: make function called changeTitle that returns a function (bounding i to the inner function) (see attached code)
	* When adding the click events to the dots, the i variable needs to be bound to that function call.  
		* Temporary solution without re-factoring: make function called movePosition that returns a function (bounding i to the inner function) (see attached code)
	
2. What you would change / Why

	A. Move CSS to separate file
		* I would move CSS to outside file because this has been a standard in web development now for many years. No inline styles!

	B. Change layout of JS file to be:

		(function($, window, document) {
		   $(function() {
		 // DOM Ready!
		   });
		}(window.jQuery, window, document));

		* This is a best practice when writing JS files as to locally scope the $, and standardize the behaviour across different environments.

	C. Hover and Click events for the dots should not use a loop, and instead something like:

		var list = $('.carousel-dots-nav-item');
		list.on('hover', 'li', function() {
		$('#title').text($(this).title);
		});

		* This solves the problems described earlier, with the scope of i being lost inside the loop.

	D. Take out the ids in the selectors, and change to classes
		* It is bad practice to use IDs in css selectors because it prevents re-usability, are over specific, and generally does not provide performance gains.  Uses classes!

	E. Use spaces instead of tabs
		* Use a text editor so all tabs get converted to spaces.  This is the standard method to make sure files look the same between systems.

	F. Add a DOCTYPE tag to beginning of HTML: <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
		* This is always a good idea so the browser can for sure know what it is dealing with.
		
	G. Move scripts to bottom of html before closing body tag.  This speeds up performance.
	
	H. I would also want to change the way the hover functionality is working.  To me, after we hover off, I think the label should resort back to what the current picture is. (add hover on / hover off functionality)



# Instructions

Please complete a code review on the JS, HTML, and CSS included with this README file.  Please include information on the following:

1. What is wrong with the JS, HTML, and CSS
2. What you would change
3. Why you would make the proposed changes

The exercise should take no longer than 30 minutes.

Thank you.